package pl.edu.agh.jtp2.home01;

import java.math.BigDecimal;

/**
 * Abstract class for IEmployee implementations
 */
public abstract class AbstractEmployee implements IEmployee {

	private final String name;
	private final String role;
    private IManager manager;
    private BigDecimal salary;

    /**
     * @param name Employee name - String
     * @param role Employee role - String
     * @param salary Employee salary - BigDecimal
     */
    public AbstractEmployee(String name, String role, BigDecimal salary) {
        this.name = name;
        this.role = role;
        this.salary = salary;
    }

    /**
     * @return Employee's manager.
     */
    public IManager getManager() {
        return manager;
    }

    /**
     * Sets Employee's manager
     * @param manager Manager instance which we want to have Employee set to.
     */
    public void setManager(IManager manager) {
        this.manager = manager;
    }

    /**
     *
     * @return Employee's role
     */
    public String getRole() {
        return role;
    }

    /**
     *
     * @return Employee's name
     */
    @Override
    public String getName() {
		return name;
	}

    /**
     *
     * @return Employee's salary
     */
    @Override
    public BigDecimal getSalary() {
        return salary;
    }

    /**
     * Leaf visitor accept method
     * @param visitor visitor to accept
     * @see pl.edu.agh.jtp2.home01.IVisitor
     * @see pl.edu.agh.jtp2.home01.IVisitable
     */
    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * Leaf implementation
     * @return Underlying tree size including itself
     */
    @Override
    public int treeSize() {
        return 1;
    }
}	
