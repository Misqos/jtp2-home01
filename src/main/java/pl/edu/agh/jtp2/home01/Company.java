package pl.edu.agh.jtp2.home01;

import java.io.Serializable;
import java.util.*;

/**
 * Created by misqos on 16.04.14.
 * Company class encloses whole company in one piece.
 */
public class Company extends AbstractCollection<IEmployee> implements Serializable, IVisitable {

    private IManager ceo;

    /**
     * Constructor.
     * @param ceo company must have CEO which implements IManager interface
     * @see pl.edu.agh.jtp2.home01.IManager
     */
    public Company(IManager ceo) {
        this.ceo = ceo;
    }

    /**
     * @param visitor visitor to accept
     * @see pl.edu.agh.jtp2.home01.IVisitor
     * @see pl.edu.agh.jtp2.home01.IVisitable
     */
    @Override
    public void accept(IVisitor visitor) {
        ceo.accept(visitor);
    }

    public IManager getCEO() {
        return ceo;
    }

    /**
     * Returns iterator to iterate through whole company like through collection
     * @return Deep First Search Iterator
     * @see pl.edu.agh.jtp2.home01.Company.DFSIterator
     */
    @Override
    public Iterator<IEmployee> iterator() {
        return new DFSIterator(ceo, this.size());
    }

    /**
     * @return size of whole company.
     */
    @Override
    public int size() {
        return ceo.treeSize();
    }

    /**
     * This iterator returns objects in DFS order.
     */
    private class DFSIterator implements Iterator<IEmployee> {

        private final IManager ceo;
        private final int size;

        private List<IEmployee> visited = new ArrayList<>();
        private IManager current = null;
        private Predicate predicate;

        /**
         * Standard iterator constructor. It returns every object in company tree.
         * @param ceo company CEO
         * @param size size of company
         */
        public DFSIterator(IManager ceo, int size) {
            this.size = size;
            this.ceo = ceo;
            predicate = new Predicate<IEmployee>() {
                @Override
                public boolean invoke(IEmployee employee) {
                    return true;
                }
            };
        }

        /**
         * This iterator returns objects for which predicate method invoke() returns true.
         * @param ceo company CEO
         * @param size size of company
         * @param predicate predicate class with invoke() method
         * @see pl.edu.agh.jtp2.home01.Predicate
         */
        public DFSIterator(IManager ceo, int size, Predicate predicate) {
            this.size = size;
            this.ceo = ceo;
            this.predicate = predicate;
        }

        /**
         * @return true if there is still object to iterate
         */
        @Override
        public boolean hasNext() {
            return visited.size() < size;
        }

        /**
         * @return next object in the order
         */
        @Override
        public IEmployee next() {
            if(current == null) {
                current = ceo;
                visited.add(ceo);
                if(predicate.invoke(current)) {
                    return ceo;
                }
            }
            for(IEmployee employee : current) {
                if(!visited.contains(employee) && employee instanceof IManager) {
                    visited.add(employee);
                    if(predicate.invoke(employee)) {
                        current = (IManager) employee;
                        return employee;
                    }
                }
                if(!visited.contains(employee)) {
                    visited.add(employee);
                    if(predicate.invoke(employee)) {
                        return employee;
                    }
                }
            }
            current = current.getManager();
            return this.next();
        }

        /**
         * This iterator do not support remove() method.
         */
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * This method adds Employee to first manager who is able to hire him
     * @param employee employee to hire
     * @return true if adding succeeded, false otherwise
     */
    @Override
    public boolean add(IEmployee employee) {
        Iterator<IEmployee> iterator = this.iterator();
        while(iterator.hasNext()) {
            IEmployee current = iterator.next();
            if(current instanceof IManager) {
                if(((IManager) current).canHire()) {
                    ((IManager) current).hire(employee);
                    return true;
                }
            }
        }
        return false;
    }
}
