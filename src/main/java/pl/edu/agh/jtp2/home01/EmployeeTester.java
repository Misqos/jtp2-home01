package pl.edu.agh.jtp2.home01;

import java.math.BigDecimal;

/**
 * Example IEmployee implementation
 * @see pl.edu.agh.jtp2.home01.IEmployee
 * @see pl.edu.agh.jtp2.home01.AbstractEmployee
 */
public class EmployeeTester extends AbstractEmployee {

    private Report report;
    private ITaskCompletionStrategy taskCompletionStrategy;

    /**
     * Standard constructor. Employee constructed like that completes whole assigned job.
     * Initially Employee has 0 units of work done.
     * @param name Employee's name
     * @param role Employee's role
     * @param salary Employee's salary
     * @see pl.edu.agh.jtp2.home01.AbstractEmployee
     * @see pl.edu.agh.jtp2.home01.ITaskCompletionStrategy
     * @see pl.edu.agh.jtp2.home01.ExactTaskCompletionStrategy
     */
    public EmployeeTester(String name, String role, BigDecimal salary) {
        super(name, role, salary);
        this.report = new Report(0);
        taskCompletionStrategy = new ExactTaskCompletionStrategy();
    }

    /**
     * In this constructor one can give Employee taskCompletionStrategy.
     * @param name Employee's name
     * @param role Employee's role
     * @param salary Employee's salary
     * @param taskCompletionStrategy The way employee does assigned tasks
     * @see pl.edu.agh.jtp2.home01.ITaskCompletionStrategy
     */
    public EmployeeTester(String name, String role, BigDecimal salary, ITaskCompletionStrategy taskCompletionStrategy) {
        super(name, role, salary);
        this.report = new Report(0);
        this.taskCompletionStrategy = taskCompletionStrategy;
    }

    /**
     * @return Work report
     * @see pl.edu.agh.jtp2.home01.Report
     */
    @Override
    public Report reportWork() {
        return this.report;
    }

    /**
     * Assigns task to Employee. Task is done in the way specified in taskCompletionStrategy.
     * Tasks are done in the moment of assignment.
     * @param task Task to perform.
     */
    @Override
    public void assign (Task task) {
        report = taskCompletionStrategy.perform(report, task);
        System.out.println(this.getName() + " testing: " + task.getWorkToDo() + " work units");
        System.out.println("Now I've done " + report.getWorkDone() + " work units");
    }

    /**
     * @return String Employee description
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("My name is" + this.getName() + ". ");
        sb.append("I am tester. My role is " + this.getRole());
        return sb.toString();
    }
}

