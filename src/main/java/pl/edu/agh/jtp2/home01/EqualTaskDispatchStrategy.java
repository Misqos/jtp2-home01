package pl.edu.agh.jtp2.home01;

import java.util.Collection;

/**
 * Created by misqos on 15.04.14.
 * ITaskDispatchStrategy implementation. Dispatches tasks equally.
 * @see pl.edu.agh.jtp2.home01.ITaskDispatchStrategy
 * @see pl.edu.agh.jtp2.home01.Task
 */
public class EqualTaskDispatchStrategy implements ITaskDispatchStrategy {
    /**
     * Main method which assigns tasks to employees.
     * @param employees Collection of employees to which task has to be assigned
     * @param task task to assign
     */
    @Override
    public void dispatch(Collection<IEmployee> employees, Task task) {
        int workToDo = task.getWorkToDo();
        int distribution = workToDo / employees.size();
        int rest = workToDo % employees.size();
        for (IEmployee employee : employees) {
            int current = 0;
            if(rest > 0) current = 1;
            Task subTask = new Task(distribution + current);
            employee.assign(subTask);
            if(rest > 0) rest--;
        }
    }
}
