package pl.edu.agh.jtp2.home01;

import java.io.*;

/**
 * Created by misqos on 22.04.14.
 * Utility class with Serialize methods.
 * @see java.io.Serializable
 */
public class FileBasedCompanyRepository {
    /**
     * Serializes company to file
     * @param company company to serialize
     * @param filename output file name
     * @throws IOException
     */
    public static void write(Company company, String filename) throws IOException {
        ObjectOutputStream objectOutput = null;
        try {
            objectOutput = new ObjectOutputStream(new FileOutputStream(filename));
            objectOutput.writeObject(company);
        } finally {
            if(objectOutput != null) {
                objectOutput.close();
            }
        }
    }

    /**
     * Reads Company object from file
     * @param filename input filename
     * @return Company object
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Company read(String filename) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInput = null;
        Company company;
        try {
            objectInput = new ObjectInputStream(new FileInputStream(filename));
            company = (Company)objectInput.readObject();
        } finally {
            if(objectInput != null) {
                objectInput.close();
            }
        }
        return company;
    }
}
