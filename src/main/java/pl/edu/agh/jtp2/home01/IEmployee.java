package pl.edu.agh.jtp2.home01;

import java.io.Serializable;
import java.math.BigDecimal;

public interface IEmployee extends IVisitable, Serializable {

	String getName();

    BigDecimal getSalary();

    IManager getManager();

    void setManager(IManager manager);

	void assign(Task task);

    public String getRole();

    int treeSize();

	Report reportWork();

}