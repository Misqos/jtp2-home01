package pl.edu.agh.jtp2.home01;

public interface IManager extends IEmployee, Iterable<IEmployee>, IVisitable {

	void hire(IEmployee employee);

	void fire(IEmployee employee);


	boolean canHire();
}
