package pl.edu.agh.jtp2.home01;

import java.io.Serializable;

/**
 * Created by misqos on 15.04.14.
 */
public interface ITaskCompletionStrategy extends Serializable {

    public Report perform(Report report, Task task);
}
