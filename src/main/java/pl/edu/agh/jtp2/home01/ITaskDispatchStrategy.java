package pl.edu.agh.jtp2.home01;

import java.io.Serializable;
import java.util.Collection;

/**
 * Created by misqos on 15.04.14.
 */

public interface ITaskDispatchStrategy extends Serializable {

    public void dispatch(Collection<IEmployee> employees, Task task);

}
