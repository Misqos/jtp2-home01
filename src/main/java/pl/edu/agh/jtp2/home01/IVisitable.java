package pl.edu.agh.jtp2.home01;

/**
 * Created by misqos on 16.04.14.
 */
public interface IVisitable {

    public void accept(IVisitor visitor);
}
