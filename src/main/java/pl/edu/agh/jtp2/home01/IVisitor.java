package pl.edu.agh.jtp2.home01;

/**
 * Created by misqos on 16.04.14.
 */
public interface IVisitor{

    public void visit(Company company);

    public void visit(IManager manager);

    public void visit(IEmployee employee);

    public String getReport();
}
