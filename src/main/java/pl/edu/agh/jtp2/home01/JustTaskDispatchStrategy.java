package pl.edu.agh.jtp2.home01;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Created by misqos on 22.04.14.
 * ITaskDispatchStrategy implementation. The higher salary - the higher task assignment.
 * @see pl.edu.agh.jtp2.home01.ITaskDispatchStrategy
 * @see pl.edu.agh.jtp2.home01.Task
 */
public class JustTaskDispatchStrategy implements ITaskDispatchStrategy {
    /**
     * Main method which dispatches task. Gets salary sum ant then dispatches task proportionally.
     * @param employees Employees collection to which task is dispatched
     * @param task task to dispatch
     */
    @Override
    public void dispatch(Collection<IEmployee> employees, Task task) {
        BigDecimal salarySum = new BigDecimal(0);
        for(IEmployee employee : employees) {
            salarySum = new BigDecimal(salarySum.intValue() + employee.getSalary().intValue());
        }
        for(IEmployee employee : employees) {
            employee.assign(new Task((int)(task.getWorkToDo()*((double)employee.getSalary().intValue()/(double)salarySum.intValue()))));
        }
    }
}
