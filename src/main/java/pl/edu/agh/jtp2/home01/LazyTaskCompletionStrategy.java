package pl.edu.agh.jtp2.home01;

/**
 * Created by misqos on 22.04.14.
 * ITaskCompletionStrategy implementation. It performs only half of the task.
 */
public class LazyTaskCompletionStrategy implements ITaskCompletionStrategy {
    /**
     * Main method which performs task.
     * @param report Current Employee's report - work units done before
     * @param task Task to perform
     * @return Report with tasks done before and part of task done recently in this method.
     */
    @Override
    public Report perform(Report report, Task task) {
        return new Report(report.getWorkDone() + task.getWorkToDo()/2);
    }
}
