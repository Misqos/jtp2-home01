package pl.edu.agh.jtp2.home01;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("What do you want to do?");
        System.out.println("1. See simple demonstration");
        System.out.println("2. Read example company from file");
        Scanner sc;
        int option;
        sc = new Scanner(System.in);
        option = sc.nextInt();
        switch (option) {
            case 1:
                demo();
                break;
            case 2:
                example();
                break;
        }
    }

    private static void demo() {
        System.out.println("Constructing CEO, making company");
        System.out.println("CEO name: Bill Gates, Salary: 10 000, Maximum number of employees: 2");
        IManager ceo = new TeamManager("Bill Gates", "CEO", new BigDecimal(10000), 2);
        System.out.println("getSalary() method effect: " + ceo.getSalary());
        Company company = new Company(ceo);
        System.out.println("Hiring two managers and 2 employees for each manager.");
        System.out.println("First manager will dispatch work equally, second one will do it fair.");
        System.out.println("Each of Manager will have one solid employee and one lazy employee");
        IManager manager1 = new TeamManager("Steve Balmer", "Developers' Manager", new BigDecimal(5000), 3, new EqualTaskDispatchStrategy());
        ceo.hire(manager1);
        IManager manager2 = new TeamManager("Steve Jobs", "Testers' Manager", new BigDecimal(4500), 3, new JustTaskDispatchStrategy());
        ceo.hire(manager2);
        IEmployee employee11 = new EmployeeDeveloper("Michał Furdyna", "Java Developer", new BigDecimal(3500), new ExactTaskCompletionStrategy());
        manager1.hire(employee11);
        IEmployee employee12 = new EmployeeDeveloper("Jan Kowalsky", "Java Developer", new BigDecimal(2000), new LazyTaskCompletionStrategy());
        manager1.hire(employee12);
        IEmployee employee21 = new EmployeeTester("Zdzisław Nowak", "Java Tester", new BigDecimal(3500), new ExactTaskCompletionStrategy());
        manager2.hire(employee21);
        IEmployee employee22 = new EmployeeTester("Bogusław Linda", "Java Tester", new BigDecimal(2000), new LazyTaskCompletionStrategy());
        manager2.hire(employee22);
        try {
            FileBasedCompanyRepository.write(company, "Company.ser");
        } catch (IOException e) {
            e.printStackTrace();
        }
        rest(company, ceo);
    }
    private static void example() {
        try {
            Company company = FileBasedCompanyRepository.read("Company.ser");
            rest(company, company.getCEO());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void rest(Company company, IManager ceo) {
        System.out.println("Now will print company tree: (Visitor)");
        IVisitor treeVisitor = new TreeStructureVisitor();
        treeVisitor.visit(company);
        System.out.println(treeVisitor.getReport());
        System.out.println("Will now assign 100 units of work to CEO.");
        ceo.assign(new Task(100));
        System.out.println("Now will make salary report.(Visitor)");
        IVisitor salaryVisitor = new SalaryReportVisitor();
        salaryVisitor.visit(company);
        System.out.println(salaryVisitor.getReport());
        System.out.println("Now will make role report. (Visitor)");
        IVisitor roleVisitor = new RoleReportVisitor();
        roleVisitor.visit(company);
        System.out.println(roleVisitor.getReport());
        System.out.println("Company as collection: ");
        for (IEmployee employee : company) {
            System.out.println(employee.getName() + " " + employee.getRole());
        }
        System.out.println("Adding to collection: ");
        IEmployee thisIsSoBoring = new EmployeeDeveloper("Demo Guy", "Lazy man", new BigDecimal(1000));
        company.add(thisIsSoBoring);
        System.out.println("Printing tree: ");
        treeVisitor = new TreeStructureVisitor();
        treeVisitor.visit(company);
        System.out.println(treeVisitor.getReport());
    }
}
