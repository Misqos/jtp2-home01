package pl.edu.agh.jtp2.home01;

/**
 * Created by misqos on 22.04.14.
 * Predicate interface for Anonymous classes.
 */
public interface Predicate<IEmployee> {
    public boolean invoke(IEmployee employee);
}
