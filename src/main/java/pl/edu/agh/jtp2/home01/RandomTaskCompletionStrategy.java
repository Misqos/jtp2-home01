package pl.edu.agh.jtp2.home01;

/**
 * Created by misqos on 22.04.14.
 * In this implementation random percentage of task is performed.
 * It will never perform 100% of task due to Math.random() method
 * which returns double value in range [0;1)
 * @see pl.edu.agh.jtp2.home01.ITaskCompletionStrategy
 * @see pl.edu.agh.jtp2.home01.Task
 */
public class RandomTaskCompletionStrategy implements ITaskCompletionStrategy {
    @Override
    /**
     * Main method which performs task.
     * @param report Current Employee's report - work units done before
     * @param task Task to perform
     * @return Report with tasks done before and part of task done recently in this method.
     */
    public Report perform(Report report, Task task) {
        return new Report( (int)(task.getWorkToDo()*Math.random()));
    }
}
