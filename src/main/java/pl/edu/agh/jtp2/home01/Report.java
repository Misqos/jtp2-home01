package pl.edu.agh.jtp2.home01;

import java.io.Serializable;

public class Report implements Serializable{

	private final int workDone;

	public Report(int workDone) {
		this.workDone = workDone;
	}

	public int getWorkDone() {
		return workDone;
	}
	 
}
