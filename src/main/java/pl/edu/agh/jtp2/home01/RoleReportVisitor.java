package pl.edu.agh.jtp2.home01;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by misqos on 22.04.14.
 * IVisitor implementation. Counts number of employees with every role in tree.
 */
public class RoleReportVisitor implements IVisitor {
    Map<String, Integer> roleMap;

    public RoleReportVisitor() {
        roleMap = new LinkedHashMap<>();
    }

    @Override
    public void visit(Company company) {
        company.accept(this);
    }

    /**
     * Inspects manager role and counts it.
     * @param manager Manager to inspect
     */
    @Override
    public void visit(IManager manager) {
        String role = manager.getRole();
        int count = roleMap.containsKey(role) ? roleMap.get(role) : 0;
        roleMap.put(role, count + 1);
    }

    /**
     * Inspects employee role ant counts it.
     * @param employee Employee to inspect.
     */
    @Override
    public void visit(IEmployee employee) {
        String role = employee.getRole();
        int count = roleMap.containsKey(role) ? roleMap.get(role) : 0;
        roleMap.put(role, count + 1);
    }

    /**
     * @return String report of visitor.
     */
    @Override
    public String getReport() {
        StringBuilder sb = new StringBuilder();
        sb.append("Role Report:\n");
        for(Map.Entry<String, Integer> entry : roleMap.entrySet()) {
            sb.append(entry.getKey()).append(" ").append(entry.getValue()).append("\n");
        }
        return sb.toString();
    }
}
