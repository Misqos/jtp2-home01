package pl.edu.agh.jtp2.home01;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by misqos on 16.04.14.
 * IVisitor implementation. Gets average salary in tree.
 */
public class SalaryReportVisitor implements IVisitor {
    private BigDecimal salarySum;
    private int numberOfEmployees;

    public SalaryReportVisitor() {
        salarySum = new BigDecimal(0);
        numberOfEmployees = 0;
    }

    @Override
    public void visit(Company company) {
        company.accept(this);
    }

    /**
     * Adds manager salary to salarySum and counts manager to numberOfEmployees
     * @param manager manager to inspect.
     */
    @Override
    public void visit(IManager manager) {
        salarySum = new BigDecimal(salarySum.intValue() + manager.getSalary().intValue());
        numberOfEmployees++;
    }

    /**
     * Adds employee salary to salarySum and counts manager to numberOfEmployees
     * @param employee employee to inspect.
     */
    @Override
    public void visit(IEmployee employee) {
        salarySum = new BigDecimal(salarySum.intValue() + employee.getSalary().intValue());
        numberOfEmployees++;
    }

    /**
     * @return String report of average salary in company.
     */
    @Override
    public String getReport() {
        StringBuilder sb = new StringBuilder();
        sb.append("Average salary is equal to ").append(salarySum.divide(new BigDecimal(numberOfEmployees), 2, RoundingMode.HALF_UP));
        return sb.toString();
    }
}
