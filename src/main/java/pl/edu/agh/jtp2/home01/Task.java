package pl.edu.agh.jtp2.home01;

import java.io.Serializable;

public class Task implements Serializable{

	private final int workToDo;

	public Task(int workToDo) {
			this.workToDo = workToDo;
	}

	public int getWorkToDo() {
            return this.workToDo;
    	}
}
