package pl.edu.agh.jtp2.home01;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Simple IManager interface implementation
 * @see pl.edu.agh.jtp2.home01.IManager
 */
public class TeamManager extends AbstractEmployee implements IManager {

	private Collection<IEmployee> employees;
	private int maxNumberOfEmployees;
    private ITaskDispatchStrategy taskDispatchStrategy;

    /**
     * Standard constructor. Manager constructed in this constructor will dispatch work equally
     * @param name Manager's name
     * @param role Manager's role
     * @param salary Manager's sallary
     * @param maxNumberOfEmployees Maximum number of employees Manager can hire
     * @see pl.edu.agh.jtp2.home01.ITaskDispatchStrategy
     * @see pl.edu.agh.jtp2.home01.EqualTaskDispatchStrategy
     */
	public TeamManager(String name, String role, BigDecimal salary, int maxNumberOfEmployees) {
        super(name, role, salary);
		this.employees = new ArrayList<IEmployee>();
        this.maxNumberOfEmployees = maxNumberOfEmployees;
        this.taskDispatchStrategy = new EqualTaskDispatchStrategy();
	}

    /**
     * In this constructor one can precise Manager's taskDispatchStrategy
     * @param name Manager's name
     * @param role Manager's role
     * @param salary Manager's salary
     * @param maxNumberOfEmployees Maximum number of employees Manager can hire
     * @param taskDispatchStrategy Strategy of task dispatch with dispatch() method
     * @see pl.edu.agh.jtp2.home01.ITaskDispatchStrategy
     */
    public TeamManager(String name, String role, BigDecimal salary, int maxNumberOfEmployees, ITaskDispatchStrategy taskDispatchStrategy) {
        super(name, role, salary);
        this.employees = new ArrayList<IEmployee>();
        this.maxNumberOfEmployees = maxNumberOfEmployees;
        this.taskDispatchStrategy = taskDispatchStrategy;
    }

    /**
     * @return true if manager can hire another employee, false otherwise
     */
    @Override
	public boolean canHire() {
		return (employees.size() < maxNumberOfEmployees);
	}

    /**
     * Hires new employee
     * @param person person to hire
     * @throws java.lang.IllegalStateException when cannot hire
     */
    @Override
	public void hire (IEmployee person) throws IllegalStateException {
		if(this.canHire()) {
            employees.add(person);
            person.setManager(this);
        } else {
            throw new IllegalStateException("Cannot hire new employee!");
        }
	}

    /**
     * @param person fires employee
     */
    @Override
	public void fire (IEmployee person) {
        employees.remove(person);
	}

    /**
     * @return Report of work done by lower part of company tree
     */
    @Override
    public Report reportWork() {
        int workDone = 0;
        for (IEmployee employee : employees) {
            employee.reportWork().getWorkDone();
        }
        return new Report(workDone);
    }

    /**
     * Dispatches task to it's employees using taskDispatchStrategy
     * @param task task to dispatch
     * @see pl.edu.agh.jtp2.home01.ITaskDispatchStrategy
     */
    @Override
    public void assign (Task task) {
        taskDispatchStrategy.dispatch(employees, task);
    }

    /**
     * @return String description.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("My name is ").append(this.getName()).append(". ")
            .append("I am manager. My role is ").append(this.getRole())
            .append(" Now i manage ").append(employees.size()).append(" employees");
        return sb.toString();
    }

    /**
     * @return iterator to iterate through manager's employees
     */
    @Override
    public Iterator<IEmployee> iterator() {
        return new EmployeesIterator(employees); //TODO make sure you can do it like that!
    }

    /**
     * Visitor support. Telling it to visit this and passes to it's children.
     * @param visitor visitor to accept
     * @see pl.edu.agh.jtp2.home01.IVisitor
     * @see pl.edu.agh.jtp2.home01.IVisitable
     */
    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
        for (IEmployee employee : employees) {
            employee.accept(visitor);
        }
    }

    /**
     * @return Underlying tree size including itself
     */
    @Override
    public int treeSize() {
        int size = 1;
        for (IEmployee employee : employees) {
            size+=employee.treeSize();
        }
        return size;
    }

    private class EmployeesIterator implements Iterator<IEmployee> {
        private Iterator iterator;

        public EmployeesIterator(Collection<IEmployee> employees) {
            this.iterator = employees.iterator();
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public IEmployee next() {
            return (IEmployee) iterator.next();
        }

        /**
         * Method remove is not supported in this iterator
         */
        @Override
        public void remove() {
            throw new UnsupportedOperationException("Method remove() not supported");
        }
    }
}
