package pl.edu.agh.jtp2.home01;

/**
 * Created by misqos on 23.04.14.
 * This IVisitor implementation builds tree representation of company.
 */
public class TreeStructureVisitor implements IVisitor {
    private StringBuilder structure;

    public TreeStructureVisitor() {
        structure = new StringBuilder();
    }

    @Override
    public void visit(Company company) {
        structure.append("Company:\n");
        company.accept(this);
    }

    @Override
    public void visit(IManager manager) {
        int counter = 0;
        IManager parent = manager.getManager();
        while(parent != null) {
            parent = parent.getManager();
            counter++;
        }
        while(counter > 0) {
            structure.append("    ");
            counter--;
        }
        structure.append(manager.getName()).append(" ").append(manager.getRole()).append("\n");
    }

    @Override
    public void visit(IEmployee employee) {
        IManager parent = employee.getManager();
        while(parent != null) {
            parent = parent.getManager();
            structure.append("    ");
        }
        structure.append(employee.getName()).append(" ").append(employee.getRole()).append("\n");
    }

    @Override
    public String getReport() {
        return structure.toString();
    }
}
