package pl.edu.agh.jtp2.home01;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by misqos on 23.04.14.
 */
public class CompanyTest {
    @Test
    public void sizeTest() throws Exception {
        //given
        IManager manager = new TeamManager("TestManager", "ceo", new BigDecimal(0), 2);
        IEmployee employee1 = new EmployeeDeveloper("Employee1", "TestGuy", new BigDecimal(10));
        IEmployee employee2 = new EmployeeDeveloper("Employee2", "TestGuy", new BigDecimal(5));

        //when
        Company company = new Company(manager);
        manager.hire(employee1);
        manager.hire(employee2);

        //then
        assertTrue(company.size() == 3);
    }

/*    @Test
    public void treeTest() throws Exception {
        //given
        IManager manager = new TeamManager("TestManager", "ceo", new BigDecimal(0), 5);
        IManager manager1 = new TeamManager("TestManager", "podrzedny1", new BigDecimal(0), 5);
        IManager manager2 = new TeamManager("TestManager", "podrzedny2", new BigDecimal(0), 5);
        IEmployee employee1 = new EmployeeDeveloper("Employee1", "TestGuy", new BigDecimal(10));
        IEmployee employee2 = new EmployeeDeveloper("Employee2", "TestGuy", new BigDecimal(5));
        IEmployee employee3 = new EmployeeDeveloper("Employee3", "TestGuy", new BigDecimal(5));
        IEmployee employee4 = new EmployeeDeveloper("Employee4", "TestGuy", new BigDecimal(5));

        //when
        Company company = new Company(manager);
        //manager.hire(employee1);
        //manager.hire(employee2);
        manager.hire(manager1);
        manager1.hire(manager2);
        manager1.hire(employee3);
        manager2.hire(employee4);

        IVisitor visitor = new TreeStructureVisitor();

        visitor.visit(company);
        System.out.println(visitor.getReport());

        //then
        assertTrue(company.size() != 0);
    }*/
}
