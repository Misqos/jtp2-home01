package pl.edu.agh.jtp2.home01;

import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;

import static org.junit.Assert.*;


/**
 * Created by misqos on 22.04.14.
 */
public class TeamManagerTest {
    @Test
    public void testHire() throws Exception {
        //given
        IManager manager = new TeamManager("Test Guy", "Manager", new BigDecimal(0), 1);
        IEmployee employee = Mockito.mock(EmployeeDeveloper.class);

        //when
        manager.hire(employee);

        //then
        assertFalse(manager.canHire());
    }

    @Test
    public void testAssign() throws Exception {
        //given
        IManager manager = new TeamManager("Test Guy", "Manager", new BigDecimal(0), 2, new JustTaskDispatchStrategy());
        IEmployee employee1 = new EmployeeDeveloper("Employee1", "TestGuy", new BigDecimal(10));
        IEmployee employee2 = new EmployeeDeveloper("Employee2", "TestGuy", new BigDecimal(5));

        //when
        manager.hire(employee1);
        manager.hire(employee2);
        manager.assign(new Task(15));

        //then
        assertTrue(employee1.reportWork().getWorkDone() == 10);
        assertTrue(employee2.reportWork().getWorkDone() == 5);
    }

    @Test
    public void testTreeSize() throws Exception {
        //given
        IManager manager = new TeamManager("Test Guy", "Manager", new BigDecimal(0), 2, new JustTaskDispatchStrategy());
        IEmployee employee1 = new EmployeeDeveloper("Employee1", "TestGuy", new BigDecimal(10));
        IEmployee employee2 = new EmployeeDeveloper("Employee2", "TestGuy", new BigDecimal(5));

        //when
        manager.hire(employee1);
        manager.hire(employee2);

        //then
        assertTrue(manager.treeSize() == 3);
    }
}
